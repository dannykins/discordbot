package funevent;

import java.util.Random;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FCoinFlip extends Command {
	
	public FCoinFlip() {
		this.name = "coinflip";
		this.aliases = new String[]{"coinflip"};
		this.help = "Flip a coin for heads or tails (or a possible third option!).";
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay
	
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		if (name2 == null) {
			name2 = name;
		}

			
		//send the opening message
		event.reply(name2 + " flipped a coin and got...");
					
		//set up a random number generator between 1% and 100% to simulate chance
		Random r = new Random();
		int flipPercent = r.nextInt(100)+1;
				
		d(2000);
					
		//if statement to determine what percentage is equal to what result
		//for here, below 49 it's tails, 50 - 98 it's heads, 99 - 100 it's a misflip
		if (flipPercent <= 49) {
			event.reply("...tails!");
		}
				
		else if (flipPercent >= 50 && flipPercent <= 98) {
			event.reply("...heads!");
		}
					
		else {event.reply("Huh... It landed on its side. Wonder what that means?");
		}

	}
}
	