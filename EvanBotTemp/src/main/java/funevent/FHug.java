package funevent;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FHug extends Command {
	
	public FHug() {
		this.name = "hug";
		this.arguments = "[name]";
		this.aliases = new String[]{"hug"};
		this.help = "Get a hug from Evan (\"-hug\") or to hug someone (\"-hug person\")!";
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay
	
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}
		
		if(event.getArgs().equalsIgnoreCase("")) {
			
			event.reply("Aww~ Bring it in, " + name2 + "~ :heart:");
			d(1000);
			event.reply("gif url");
		}
		
		else {
			
			try {
				String[] hug = event.getArgs().split(" ");
				String hugee = (hug[0]);
				event.reply("Aww~ A hug from " + name2 + " to " + hugee +"~ :heart:");
				d(1000);
				event.reply("gif url");
				
			}catch(Exception e) {
				event.reply("Oops, something went wrong. I've forgotten how to hug!");
			}
		}
	}

}
