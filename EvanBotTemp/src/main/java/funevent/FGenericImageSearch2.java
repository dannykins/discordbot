package funevent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.api.entities.TextChannel;

public class FGenericImageSearch2 extends Command {
	
	//this command assumes log-in. Search1 is for open instances.
	
	private static final boolean TRUE = false;

	public FGenericImageSearch2() {
		this.name = "search2";
		this.arguments = "[tags]";
		this.aliases = new String[]{"search2"};
		this.help = "Returns a random image based on tags given";
	}
    
	@Override
    //end pasted items
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}
		
		//grab the channel the message is sent in
		String channel = event.getTextChannel().getId();
		

		String rawterms = event.getArgs();
		String rawterms2 = rawterms.replace(" ", ",");
		String searchterms = "&text="+ rawterms2;
		String rating_name = "";
		
		String botuser = "guest";
		String botpass = "guest";
		String loginlink = "login page url part 1" + botuser +"&password=" + botpass;
		String sid = "";
		String tags = "";
		String searchlink = "";
		String filelink = "";
		String submissionobject = "";
		ArrayList<String> filelinks = new ArrayList<String>();
		String finalfilelink = "";
		String titles = "";
		
		boolean imagechannel = true;
		if (channel.contains("258660068415766538") || channel.contains("259046537798090752") || channel.contains("504366103133159444")) {
			sfwchannel = true;
			searchterms = "&text="+ rawterms2 + ",image";
		}else {
			sfwchannel = false;
			searchterms = "&text="+ rawterms2;
		}

		
		JSONParser parser = new JSONParser();
		
		//login
		
		try {
			URL loginurl = new URL(loginlink);
			BufferedReader reader = new BufferedReader(new InputStreamReader(loginurl.openConnection().getInputStream()));
			
			String lines;
			
			while((lines = reader.readLine()) != null){
				JSONArray array = new JSONArray();
				array.add(parser.parse(lines));
				
				for(Object o : array) {
					JSONObject object = (JSONObject) o;
					
					sid = (String) object.get("sid");
				}
			}
			
			reader.close();
			
			System.out.println(sid);
			
		
			try {
				searchlink = "api url 1"+ sid + searchterms +"&orderby=create_datetime_usertime&type=1,2,3,4,6,7,8,9";
				URL searchurl = new URL(searchlink);
				BufferedReader reader2 = new BufferedReader(new InputStreamReader(searchurl.openConnection().getInputStream()));
				
				Object test = new JSONParser().parse(reader2);
					
				JSONObject test2 = (JSONObject) test;
				
				JSONArray allsubmissions = (JSONArray) test2.get("submissions");
				
				for(Object obj : allsubmissions) {
					JSONObject subobj = (JSONObject) obj;
					
					filelink = (String) subobj.get("file_url_full");
					
					filelinks.add(filelink);
				}

				int randomnumber = (int) ((Math.random() * filelinks.size()) + 1);
			
				finalfilelink = (String) filelinks.get(randomnumber);
			
				event.reply(finalfilelink);
			
			
			} catch (Exception e) {

				event.reply("Heck, I couldn't grab an image for you! Perhaps try different tags?");
			}

		} catch (Exception e) {

			event.reply("Oh no, I couldn't log in!");
		}
    }
		

}
