package funevent;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FHello extends Command {
	
	public FHello() {
		this.name = "hello";
		this.aliases = new String[]{"hello"};
		this.help = "Say hi to the bot! :smiley:";
	}
	
	@Override
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}


		//Arrays with possible Bot reactions with related random number generators to randomize the possible responses
		String[] responses = {"Hey there, ", "Hi there, ", "Hey, ", "Hi, ", "Yo, ", "Hiya, ", ""};
		String[] emoji = {"!", ":hugging:",":wave:", ":thumbsup:", ":kissing_heart:", " ", " ", ":hugging:", ":smile:", ":smiley:",":smiley_cat:", ":slight_smile:"};
		String[] punctuation = {" ", "! ","~ ","! ","~ ",".","! "};
		int randomNumber0 = (int) ((Math.random() * responses.length));
		int randomNumber1 = (int) ((Math.random() * emoji.length));
		int randomNumber2 = (int) ((Math.random() * punctuation.length));
				
		//combine all the random parts to a coherent message in the same channel the original message was sent!
		event.reply(responses[randomNumber0] + name2 + punctuation[randomNumber2] + emoji[randomNumber1]);
	}
}
