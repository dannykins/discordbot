package funevent;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FDiceRoll eextends Command {
	
	public FDiceRoll() {
		this.name = "roll";
		this.arguments = "[number]d[sides]";
		this.aliases = new String[]{"roll"};
		this.help = "Roll a #d# di/ce! (e.g. \"roll 2d20\").";
	}
	
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		if (name2 == null) {
			name2 = name;
		}
			
		//make sure the message includes the #-sided dice and the number of rolls desired
		if (event.getArgs().equalsIgnoreCase("")) {
			event.reply("Sorry, I don't understand. Please try again but use this format \\\"-roll 1d12\\\" so I can make it work! :smile:");
		}
		else {
			
			try {
				String[] rawDice = event.getArgs().split("d");
				int number = Integer.parseInt(rawDice[0]);
				int sides = Integer.parseInt(rawDice[1]);
				int roll = (int) ((Math.random() * sides) + 1) * number;
				event.reply(name2 + " rolled a D" + sides + " die "	+ number + " times and got a total number of " + roll);
				
			}catch(Exception e) {
				event.reply("Oops, something went wrong. I can't find my dice!");
			}
		}
	}
}
		

