package funevent;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FHowAreYou extends Command {
	
	public FHowAreYou() {
		this.name = "howareyou";
		this.aliases = new String[]{"howareyou"};
		this.help = "Ask the Bot how he's doing.";
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay
	
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}
			
		//grab the channel the message is sent in
		String channel = event.getTextChannel().getId();
			
		// If statement so the Bot only reacts if it's being pinged
		if (channel.contains("258660068415766538")) {
						
			//Array with possible Bot reactions with related random number generators to randomize the possible responses
			String[] HowAreYou = {"Oh, I suppose I can't complain!", 
					"Enjoying the rain outside!", 
					"It's a little cold, but I'm doing all right~", 
					"Feeling a little hungry...", 
					"Ughhhhhh...... I'm so boredddddddddd.....",
					"Probably gonna do some exercising in a bit.",
					"Feeling super lazyyyyyyyyy.............",
					"I'm doing pretty good! :smiley:",
					"I feel grrrrrrrrr-eat!",
					"I'm feeling pretty purr-fect~",
					"Feline fine~",
					"Not too shabby~",
					"Feeling fine! :smile:",
					"Kinda tired, but I'll make it~",
					"Oof, stayed up too late! But otherwise just fine~"};
			int randomNumber = (int) ((Math.random() * HowAreYou.length));
			event.reply(HowAreYou[randomNumber]);
			d(2000);
			event.reply("How about you, "+ name2 + "~?");

		}

		else {
			event.reply("If you don't mind, ask me again in one of the text channels~ :smiley:");
		}
	}
}
