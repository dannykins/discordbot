package funevent;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FInvite extends Command {
	
	public FInvite() {
		this.name = "invite";
		this.aliases = new String[]{"invite"};
		this.help = "Creates a server invite link for you to share.";
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay
	
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}
			
		//event.getChannel().sendMessage(name + " sent the following message in " +  channel + ": " + message).queue();
		event.reply("Let me snag you an invite code. Gimme a sec!");
		d(3000);
		event.reply("All right, " + name2 + ", here is the invite link for you to use:\n"
				+ "url link");
					
	}
				
}
