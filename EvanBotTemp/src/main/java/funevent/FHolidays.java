package funevent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

public class Holidays extends Command {
	
	private EventWaiter waiter;
	
	public Holidays(EventWaiter waiter) {
		this.name = "day";
		this.aliases = new String[]{"day"};
		this.help = "Brings up any holidays happening today";
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay
	
	@Override
    //end pasted items
	protected void execute(CommandEvent event) {
		
		Calendar date = new GregorianCalendar();
		int year = date.get(Calendar.YEAR);
		int month = date.get(Calendar.MONTH) + 1;
		int day = date.get(Calendar.DAY_OF_MONTH);
		int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
		String[] dayNames = {"Blank lol","Sunday", "Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday"};
		String[] monthNames = {"January", "February","March","April","May","June","July","August","September","October","November","December"};
		String datelink = "https://calendarific.com/api/v2/holidays?&api_key=e84e07286c218534a6fc035d1189198be6c6e06a&country=US&year=" + year + "&month=" + month + "&day=" + day;
		
		try {
			URL dateurl = new URL(datelink);
			BufferedReader reader = new BufferedReader(new InputStreamReader(dateurl.openConnection().getInputStream()));
			
			JSONObject object = (JSONObject) new JSONParser().parse(reader);
			
			JSONObject response = (JSONObject) object.get("response");
			
			JSONArray holidays = (JSONArray) response.get("holidays");
			
			JSONObject holiday1 = (JSONObject) holidays.get(0);
			
			event.reply("Today is " + dayNames[dayOfWeek] + ", " + monthNames[month] + " " + day + ", " + year +".");
			if(holidays.size()==1) {
				event.reply("Happy " + holiday1 + "!");
				d(1000);
				event.reply((String) holiday1.get("description"));
			}
			else if(holidays.size()>0) {
				event.reply("There are " + holidays.size() + " holidays today!");
				event.reply("First up is " + holiday1.get("name") + ".");
				d(1000);
				event.reply((String) holiday1.get("description"));
				
				for (int fill = 1; fill < holidays.size(); fill++) {
					d(1000);
					JSONObject holidayfill = (JSONObject) holidays.get(fill);
					event.reply("Today is also " + holidayfill.get("name") + ".");
					d(1000);
					event.reply((String) holidayfill.get("description"));
				}
			}
			
//			event.reply("Source: calendarific.com");
			
//			if(holidays.size()>0) {
//			event.reply("Would you like to know more? Just reply with a yes, if so~");
//			
//			waiter.waitForEvent(GuildMessageReceivedEvent.class, e1 -> e1.getAuthor().equals(event.getAuthor()) && e1.getChannel().equals(event.getChannel()), e1 -> {
//				try {
//					String message = e1.getMessage().getContentRaw().toLowerCase();
//					
//					System.out.println("I got your message!");
//					
//					if (message.contains("yes")) {
//						
//						System.out.println("You said yes!");
//						for (int a = 0; a < holidays.size(); a++) {
//							JSONObject description = (JSONObject) holidays.get(a);
//							System.out.println("Getting the holiday number " + a);
//							event.reply(description.get("name") + ": " + description.get("description"));
//						}
//					}else {
//						System.out.println("They said no ):");
//					}
//					
//				}catch (Exception e) {
//				System.out.println("something happened lol.");
//				}
//			},
//			//if there's too long a delay in getting a response, end the event!
//			1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long!"));}
			
		}catch (Exception e) {

			event.reply("Uh-oh, something went wrong on my end...");
		}
		
	}

}
