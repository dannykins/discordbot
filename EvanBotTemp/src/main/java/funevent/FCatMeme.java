package funevent;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FCatMeme extends Command {
	
	public FCatMeme() {
		this.name = "catpic";
		this.aliases = new String[]{"catpic"};
		this.help = "The bot shares a cat meme.";
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay

	protected void execute(CommandEvent event) {
		
			String channel = event.getTextChannel().getId();
					
					//if statement to make sure this isn't sent in non-spam channel
					if (channel.contains("270371462093733889") || channel.contains("308666585407815688") || channel.contains("504325943645569044")) {

						// Begin three-part message response
						// Message 1, arrays of possible replies, random number generator and message posting
						String[] starter = { "Who's ready for a meme?", "One cat meme, coming right up~", "Here comes a purr-fect meme for your enjoyment~" };
						int rn2 = (int) ((Math.random() * starter.length));
						event.reply(starter[rn2]);

						d(3000);

						// Message 2, arrays of possible replies, random number generator and message posting
						String[] imgur = { "7bcCuyx.png", "RLXWnQp.jpg", "qf9JyPC.png", "0o9f4y8.png", "9DjTxX6.png"};
						String[] banter = { ":smile:", ":smiley:", ":laughing:", ":rofl:"};
						int rn0 = (int) ((Math.random() * imgur.length));
						int rn1 = (int) ((Math.random() * banter.length));
						event.getChannel().sendMessage(banter[rn1]).queue();
						event.reply("https://i.imgur.com/" + imgur[rn0]);
					}
					
					//if it was sent in a non-spam channel
					else {
						event.reply("This isn't the right channel for that sort of thing! :smile:");
					
					}

							
				}
			}
		}
		
	}

}
