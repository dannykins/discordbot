package funevent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class FGenericImageSearch1 extends Command {
	
	//this command assumes no log-in. Search2 is for login instances.
	
	public FGenericImageSearch1() {
		this.name = "search1";
		this.arguments = "[tags]";
		this.aliases = new String[]{"search1"};
		this.help = "Returns a random image based on tags given (or a random image if there are not tags given!)";
	}
    
	@Override
    //end pasted items
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}
		//grab the channel the message is sent in
		String channel = event.getTextChannel().getId();
		
		String searchTerms = event.getArgs();
		
		String searchlink = "Insert Search Page Url Here" + searchTerms;
		String imagelink = "Insert Image Page Ur Here";
		ArrayList<String> directoryArray = new ArrayList<String>();
		ArrayList<String> imageArray = new ArrayList<String>();
		String directory;
		String image;
		String finalDirectory;
		String finalImage;
		
		JSONParser parser = new JSONParser();
		try {
			URL searchurl = new URL(searchlink);
			BufferedReader reader = new BufferedReader(new InputStreamReader(searchurl.openConnection().getInputStream()));
			
			String lines = "";
			
			while((lines = reader.readLine()) != null){
				
				JSONArray newarray = (JSONArray) parser.parse(lines);
				
				for (Object o : newarray) {
					JSONObject object = (JSONObject) o;
					
					directory = (String) object.get("directory");
					image = (String) object.get("image");
										
					directoryArray.add(directory);
					imageArray.add(image);
					
				}
			}

			reader.close();
			
			int randomnumber = (int) ((Math.random() * directoryArray.size()) + 1);
			
			finalDirectory = (String) directoryArray.get(randomnumber);
			finalImage = (String) imageArray.get(randomnumber);
			
//			if statement to make sure this isn't sent in non-image channel
			if (channel.contains("258660068415766538") || channel.contains("259046537798090752") || channel.contains("504366103133159444")) {
				event.reply("Oh, this command is for images, so I'll be posting it in the image spam channel.");
			
			event.reply(imagelink + directoryArray.get(randomnumber) + "/" + imageArray.get(randomnumber));
			}
			else {
				event.reply(imagelink + directoryArray.get(randomnumber) + "/" + imageArray.get(randomnumber));
			}

			
		} catch (Exception e) {

			event.reply("Woah, something went wrong!");
			if (searchTerms.contains("Insert any words you don't want used here")) {
				event.reply("You've tried to search for a disallowed word.");
			}
			event.reply("There might be no images with those tags available. Sorry about that!");
		}

    }
		

}
