package funevent;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

public class Madlibs extends Command {
	
	private EventWaiter waiter;
	
	public Madlibs(EventWaiter waiter) {
		this.name = "madlib";
		this.aliases = new String[]{"madlib"};
		this.help = "Play a round of madlibs (WIP!).";
		this.cooldown = 10;
		this.waiter = waiter;
	}
	
	// provides an optimizable time delay
	public static void d(int delaytime) {
		try {
			Thread.sleep(delaytime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
	//end of the delay
    
    @Override
    //end pasted items
	protected void execute(CommandEvent event) {
		
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		//System.out.println(name2);
		if (name2 == null) {
			name2 = name;
		}
		
		event.reply("All right! Let's play a round of Madlibs~");
		d(1000);
		event.reply("I'll craft you a story, but I need some words from you first~");
		d(1000);
		event.reply("So give me four nouns, two verbs, one place, and two pronouns, separated by spaces (e.g. dog cat cup run swim store he she).");
		event.reply("(Remember to check your spelling before hitting enter~!)");
		event.reply("(Type STOP if you'd like to stop playing~)");
		
		waiter.waitForEvent(GuildMessageReceivedEvent.class, e1 -> e1.getAuthor().equals(event.getAuthor()) && e1.getChannel().equals(event.getChannel()), e1 -> {
			try {
				String message = e1.getMessage().getContentRaw().toLowerCase();
				
				if (message.contains("stop")) {
					event.reply("No worries. The game has ended!");
				}
				else {
					String[] word = message.split(" ");
					String noun1 = word[0];
					String noun2 = word[1];
					String noun3 = word[2];
					String noun4 = word[3];
					String verb1 = word[4];
					String verb2 = word[5];
					String place1 = word[6];
					String pronoun1 = word[7];
					String pronoun2 = word[8];
			
					//event.reply("You've said: " + noun1 + ", " + noun2 + ", and " + noun3 + ".");
					d(1000);
					event.reply("And now Evanbot proudly presents:");
					d(1000);
					event.reply("**The " + noun1.substring(0, 1).toUpperCase()+noun1.substring(1) + " and the " + noun2.substring(0, 1).toUpperCase()+noun2.substring(1) + "**");
					d(1000);
					//event.reply(noun1.substring(0, 1).toUpperCase()+noun1.substring(1) + " loved playing a rousing game of " + noun2 + " with his friend " + noun3 + ".");
					event.reply("A " + noun1 + ", returning without protection from the " + place1 + ", was pursued by a " + noun2 + "."
							+ " Seeing " + pronoun1 + " could not escape, " + pronoun1 + " turned round, and said:  \"I know, friend " + noun2 + ", that I must be your " + noun4 + ", but before I die I would ask of you one favor you will play me a tune to which I may dance.\"  \n"
							+ "The " + noun2 + " complied, and while he was " + verb1 + "ing and the " + noun1 + " was " + verb2 + ", some " + noun3 + "s hearing the sound ran up and began chasing the " + noun2 + ".  Turning to the " + noun1 +", " + pronoun2 + " said, \"It is just what I deserve; for I, who am only a butcher, should not have turned piper to please you.\"");
					event.reply("I only have this one story to use right now, but I hope you had fun! :smiley:");
				}
			}
			catch(Exception e) {
				event.reply("Oops, something went wrong... Did you type things correctly?");
			}

		});
	
			
			
	}
		
}
