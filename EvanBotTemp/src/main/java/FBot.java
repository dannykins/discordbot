import javax.security.auth.login.LoginException;

import filter.BannedWords;
import funevent.FCoinFlip;
import funevent.FDiceRoll;
import funevent.FCatMeme;
import funevent.FHello;
import funevent.Holidays;
import funevent.FHowAreYou;
import funevent.FHug;
import funevent.FInvite;
import funevent.FLick;
import funevent.FMadlibs;
import funevent.FHolidays;
import funevent.FGenericImageSearch1;
import funevent.FGenericImageSearch2;

import com.jagrosh.jdautilities.command.*;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;

import backgroundevents.FBotEmbed;
import backgroundevents.FPingEvent;
import backgroundevents.FServerInfo;
import backgroundevents.FTestEvent;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.events.*;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Bot extends ListenerAdapter {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws LoginException, InterruptedException {

		//set up an eventwaiter
		EventWaiter waiter = new EventWaiter();
		
		//The new JDA-Utilities method!
		CommandClientBuilder command = new CommandClientBuilder();
		command.setOwnerId("259058842820804609");
		command.setPrefix("-");
		command.setHelpWord("help");
		command.addCommands(
				new FServerInfo(),
				new FBotEmbed(),
				new FCateMeme(),
				new FCoinFlip(),
				new FGenericImageSearch1(),
				new FGenericImageSearch2(),
				new FHolidays(waiter),
				new FDiceRoll(),
				new FHello(),
				new FHowAreYou(),
				new FHug(),
				new FInvite(),
				new FMadlibs(),
		
		CommandClient client = command.build();
		
		// this builds the bot itself
		new JDABuilder(AccountType.BOT)
			.setToken("MjU5MDU4ODQyODIwODA0NjA5.Xs1kig.jwQ94z5AUfMfgGP6N5DrX2MVI_A")
		//The below events are with the older non-JDA style. So have shifted over the non-testing events to the new JDA-Utilities!
			.addEventListeners(new FBot())
			.addEventListeners(new FPingEvent())
			.addEventListeners(new FTestEvent())
			.addEventListeners(new FBannedWords())
			.addEventListeners(client)
			.addEventListeners(waiter)
			.build();
		
		
	}

	// this is "turning the bot on"
	public void onReady(ReadyEvent event) {

		System.out.println("The bot is online and working!");
		System.out.println(event.getJDA().getToken());
	}
	
}
