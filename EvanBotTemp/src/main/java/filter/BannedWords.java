package filter;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class BannedWords extends ListenerAdapter{
	
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		
		//grab the message itself and split it into an array
		String[] message = event.getMessage().getContentRaw().toLowerCase().split(" ");
		//grab the channel the message is sent in
		String channel = event.getTextChannel().getId();
		//grab the author who sent the message and using a nickname, if available
		String name = event.getAuthor().getName();
		String name2 = event.getMember().getNickname();
		if (name2 == null) {
			name2 = name;
		}
		
		//list of nono words
		String[] BannedWords = {"nigga", "nigger", "fag", "faggot"};

		for (int i = 0; i < message.length; i++) {
				
			for(int c = 0; c < BannedWords.length; c++) {
						
				//using banned words. in the future, move this for all channels!!
				if(message[i].equals(BannedWords[c])) { 
							
					event.getChannel().sendMessage("That's a banned word on this server. This is your warning to never use it again.").queue();
					event.getChannel().sendMessage("The server owner has been informed of this.").queue();
					event.getChannel().sendMessage("Further use of that word will get you banned.").queue();
				}
			}
		
		}
	}
	
}

