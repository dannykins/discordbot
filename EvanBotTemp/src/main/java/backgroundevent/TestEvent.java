package event;


import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class TestEvent extends ListenerAdapter{
	
	public void onMessageReceived(MessageReceivedEvent event) {
		
		//if loop to keep the Bot from replying to its own messages!
		if (!event.getAuthor().isBot()) {
			
			//grab the raw message and convert it to a lower case string so there's no need to worry about punctuation or capitalization
			String message = event.getMessage().getContentRaw().toLowerCase();

			
			//grab the author who sent the message and using a nickname, if available
			String name = event.getAuthor().getName();
			String name2 = event.getMember().getNickname();
			//System.out.println(name2);
			if (name2 == null) {
				name2 = name;
			}

	
			
			//grab the channel the message is sent in
			String channel = event.getTextChannel().getId();
			
			//System.out.println(channel);
			
			boolean evanbot = message.contains("259058842820804609");
			boolean evanbot2 = message.contains("0evanbot");
			
			//If statement so the Bot only reacts if it's being pinged
			if (evanbot || evanbot2) {			

				//If statement so the Bot only reacts if it's being pinged
				if (message.contains("run")) {
					
					//event.getChannel().sendMessage(name + " sent the following message in " +  channel + ": " + message).queue();
					//System.out.println(name + " sent the following message in " +  channel + ": " + message);
					event.getChannel().sendMessage(name + " who is nicknamed " + name2 + " sent the following message in " +  channel + ": " + message).queue();
				}
				
			}
		}
		
	}

}
