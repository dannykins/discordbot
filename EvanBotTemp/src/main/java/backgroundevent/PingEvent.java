package event;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class PingEvent extends ListenerAdapter{
	
	public void onMessageReceived(MessageReceivedEvent event) {
		
		//if loop to keep the Bot from replying to its own messages!
		if (!event.getAuthor().isBot()) {
			//grab the raw message and convert it to a lower case string so there's no need to worry about punctuation or capitalization
			String message = event.getMessage().getContentRaw().toLowerCase();
			//set up the callsigns for the Bot's name
			boolean evanbot = message.contains("259058842820804609");
			boolean evanbot2 = message.contains("0evanbot");
			
			//If statement so the Bot only reacts if it's being pinged
			if (evanbot || evanbot2) {
				//if statement with the words that can prompt the reaction
				if (message.contains("ping")) {
				
					//Reply with "pong" in the same channel the original message was sent!
					event.getChannel().sendMessage("pong!").queue();
							
				}
			}
		}
		
	}

}
