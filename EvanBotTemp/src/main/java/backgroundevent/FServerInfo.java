package backgroundevents;

import java.awt.Color;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.api.EmbedBuilder;

public class FServerInfo extends Command {
	
	public ServerInfo() {
		this.name = "serverinfo";
		this.aliases = new String[]{"server"};
		this.help = "Gives information about the server.";
	}

	protected void execute(CommandEvent event) {
		
		EmbedBuilder embed = new EmbedBuilder() //create the embed
				.setTitle("Discord Server Information") //add a title
				.setColor(Color.GREEN)
				.setAuthor(event.getGuild().getName())
				//.setImage("image url")
				.setThumbnail("thumbnail image url")
				.addField("**Welcome!**","This server is created by User. Feel free to hang out and chat!", true)
				.addField("Check out url link:", "url",true)
				.addField("Owner: ", event.getGuild().getOwner().getEffectiveName(), true)
				.addField("Member Count:", Integer.toString(event.getGuild().getMembers().size()), true);
		event.reply(embed.build()); //send the embed
	}
	
}
