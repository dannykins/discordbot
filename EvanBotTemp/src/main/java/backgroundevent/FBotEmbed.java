package backgroundevents;

import java.awt.Color;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.api.EmbedBuilder;

public class FBotEmbed extends Command {
	
	public FBotEmbed() {
		this.name = "botinfo";
		this.aliases = new String[]{"bot"};
		this.help = "Gives information about the bot.";
	}

	protected void execute(CommandEvent event) {
		
			EmbedBuilder embed = new EmbedBuilder() //create the embed
				.setTitle("Bot: Evan") //add a title
				.setColor(Color.MAGENTA)
				//.setImage("image url")
				//.setThumbnail("thumbnail url")
				.addField("About Me:","Hey there! I'm a Bot! My favorite color is purple. I'm still figuring out things, but I'm happy to be here!", true);
			event.reply(embed.build()); //send the embed

	}
}
